Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: snd
Upstream-Contact: IOhannes m zmölnig <umlaeute@debian.org>
Source: https://ccrma.stanford.edu/software/snd/

Files: *
Copyright: 1996-2022 Bill Schottstaedt (bil@ccrma.stanford.edu)
License: other-Tcl/Tk

Files: bess.rb
 dlocsig.rb
 inf-snd.el
 sndins/sndins.c sndins/sndins.h
 ws.rb
Copyright: 2002-2020, Michael Scholz <mi-scholz@users.sourceforge.net>
License: BSD-2-clause

Files: gl2ps.c
 gl2ps.h
Copyright: 1999-2012, C. Geuzaine
License: GPL-2+ or GL2PS

Files: bess1.rb
Copyright: 2002-2009, Michael Scholz
License: GPL-2+

Files: CM_patterns.scm
Copyright: 2008-2009, Rick Taube
License: LLGPL

Files: s7-slib-init.scm
Copyright: n/a
License: public-domain

Files: dlocsig.scm
Copyright: 1992-2001, Fernando Lopez Lezcano
License: other-Tcl/Tk

Files: DotEmacs
Copyright: 2001, Fernando Lopez-Lezcano <nando@ccrma.stanford.edu>
License: other-Tcl/Tk

Files: snd-ladspa.c
Copyright: 2000, Richard W.E. Furse
License: other-Tcl/Tk

Files: rhypar.cms
Copyright: 2007, Juan Reyes <juanig AT ccrma.Stanford DOT edu>
License: other-Tcl/Tk

Files: debian/*
Copyright: 2016-2022, IOhannes m zmölnig <umlaeute@debian.org>
 2009-2012, Alessio Treglia <quadrispro@ubuntu.com>
 2011, Reinhard Tartler
License: other-Tcl/Tk

Files: debian/missing-sources/jqconsole.coffee
Copyright: 2011-2014, Max Shawabkeh
 2011-2014, Amjad Masad
License: Expat

License: other-Tcl/Tk
 The authors hereby grant permission to use, copy, modify, distribute,
 and license this software and its documentation for any purpose.  No
 written agreement, license, or royalty fee is required.  Modifications
 to this software may be copyrighted by their authors and need not
 follow the licensing terms described here.
 .
 IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
 DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 .
 THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
 IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
 NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
 MODIFICATIONS.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2, or (at your option) any
 later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <https://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the complete text of the GNU General Public License
 (GPL) version 2 can be found in "/usr/share/common-licenses/GPL-2".


License: LLGPL
 This program is licensed under the terms of the Lisp Lesser GNU
 Public License, known as the LLGPL.  The LLGPL consists of a preamble
 and the LGPL.  Where these conflict, the preamble takes precedence.
 .
 Preamble:
 .
 The concept of the GNU Lesser General Public License version 2.1
 ("LGPL") has been adopted to govern the use and distribution of
 above-mentioned application. However, the LGPL uses terminology that
 is more appropriate for a program written in C than one written in
 Lisp. Nevertheless, the LGPL can still be applied to a Lisp program
 if certain clarifications are made. This document details those
 clarifications. Accordingly, the license for the open-source Lisp
 applications consists of this document plus the LGPL. Wherever there
 is a conflict between this document and the LGPL, this document takes
 precedence over the LGPL.
 .
 A "Library" in Lisp is a collection of Lisp functions, data and
 foreign modules. The form of the Library can be Lisp source code (for
 processing by an interpreter) or object code (usually the result of
 compilation of source code or built with some other
 mechanisms). Foreign modules are object code in a form that can be
 linked into a Lisp executable. When we speak of functions we do so in
 the most general way to include, in addition, methods and unnamed
 functions. Lisp "data" is also a general term that includes the data
 structures resulting from defining Lisp classes. A Lisp application
 may include the same set of Lisp objects as does a Library, but this
 does not mean that the application is necessarily a "work based on the
 Library" it contains.
 .
 The Library consists of everything in the distribution file set before
 any modifications are made to the files. If any of the functions or
 classes in the Library are redefined in other files, then those
 redefinitions ARE considered a work based on the Library. If
 additional methods are added to generic functions in the Library,
 those additional methods are NOT considered a work based on the
 Library. If Library classes are subclassed, these subclasses are NOT
 considered a work based on the Library. If the Library is modified to
 explicitly call other functions that are neither part of Lisp itself
 nor an available add-on module to Lisp, then the functions called by
 the modified Library ARE considered a work based on the Library. The
 goal is to ensure that the Library will compile and run without
 getting undefined function errors.
 .
 It is permitted to add proprietary source code to the Library, but it
 must be done in a way such that the Library will still run without
 that proprietary code present. Section 5 of the LGPL distinguishes
 between the case of a library being dynamically linked at runtime and
 one being statically linked at build time. Section 5 of the LGPL
 states that the former results in an executable that is a "work that
 uses the Library." Section 5 of the LGPL states that the latter
 results in one that is a "derivative of the Library", which is
 therefore covered by the LGPL. Since Lisp only offers one choice,
 which is to link the Library into an executable at build time, we
 declare that, for the purpose applying the LGPL to the Library, an
 executable that results from linking a "work that uses the Library"
 with the Library is considered a "work that uses the Library" and is
 therefore NOT covered by the LGPL.
 .
 Because of this declaration, section 6 of LGPL is not applicable to
 the Library. However, in connection with each distribution of this
 executable, you must also deliver, in accordance with the terms and
 conditions of the LGPL, the source code of Library (or your derivative
 thereof) that is incorporated into this executable.
Comment:
 The LLGPL is also available online at
 https://opensource.franz.com/preamble.html
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License (version 2.1) can be found in /usr/share/common-licenses/LGPL-2.1.


License: GL2PS
 GL2PS LICENSE
 Version 2, November 2003
 .
 Copyright (C) 2003, Christophe Geuzaine
 .
 Permission to use, copy, and distribute this software and its
 documentation for any purpose with or without fee is hereby granted,
 provided that the copyright notice appear in all copies and that both
 that copyright notice and this permission notice appear in supporting
 documentation.
 .
 Permission to modify and distribute modified versions of this software
 is granted, provided that:
 .
 1) the modifications are licensed under the same terms as this
 software;
 .
 2) you make available the source code of any modifications that you
 distribute, either on the same media as you distribute any executable
 or other form of this software, or via a mechanism generally accepted
 in the software development community for the electronic transfer of
 data.
 .
 This software is provided "as is" without express or implied warranty.

License: public-domain
 Author: Aubrey Jaffer
 This code is in the public domain.

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use, copy,
 modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR
 ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
